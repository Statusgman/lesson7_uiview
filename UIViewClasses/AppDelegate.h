//
//  AppDelegate.h
//  UIViewClasses
//
//  Created by Alexander Dupree on 19/04/2017.
//  Copyright © 2017 SBTiOSSchool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


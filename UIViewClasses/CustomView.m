//
//  CustomView.m
//  UIViewClasses
//
//  Created by Alexander Dupree on 19/04/2017.
//  Copyright © 2017 SBTiOSSchool. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView

#pragma mark - Initialization

- (instancetype)init {
    return [super init];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"Wow! I've been initialized via code!");
        self.backgroundColor = [UIColor purpleColor];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSLog(@"Wow! I've been initialized via IB!");
        self.backgroundColor = [UIColor cyanColor];
    }
    return self;
}

//#pragma mark - Drawing

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

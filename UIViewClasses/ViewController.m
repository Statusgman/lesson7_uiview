//
//  ViewController.m
//  UIViewClasses
//
//  Created by Alexander Dupree on 19/04/2017.
//  Copyright © 2017 SBTiOSSchool. All rights reserved.
//

#import "ViewController.h"
#import "CustomView.h"

@interface ViewController ()

@property (nonatomic, strong) CustomView *someView;

@end

@implementation ViewController

#pragma mark - View life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.someView = [CustomView new];
    [self.view addSubview:self.someView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    CGFloat offset = 20;
    CGFloat x = offset;
    CGFloat y = offset;
    CGFloat width = self.view.frame.size.width - 2*offset;
    CGFloat height = self.view.frame.size.height - 2*offset;
    CGRect rect = CGRectMake(x, y, width, height);
    self.someView.frame = rect;
}

@end
